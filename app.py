from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_pymongo import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

CONNECTION_STRING = "mongodb+srv://root:root@cluster0-holdv.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('todolist')
user_collection = pymongo.collection.Collection(db, 'todo')

app = Flask(__name__)

# Route racine
@app.route("/")
def index():
    return render_template('index.html')

@app.route('/task', methods=['POST'])
def task():

    # Create a task
    nom=request.form['name']
    desc=request.form['desc']
    date=request.form['date']
    db.task.insert_one({'task': nom, 'description': desc, 'date':date})
    return redirect('/list')

@app.route("/list", methods=['GET'])    
def liste():    
    #Display the all Tasks
        
    tasks=db.task

    result=[]

    for field in tasks.find():
        result.append({'_id': (field['_id']), 'nom': field['task'], 'desc': field['description'], 'date': str(field['date']) })

    # print(result)

    return render_template('liste.html', result=result)

@app.route('/del_task', methods=['POST'])
def delete():
    # Delete a task

    result = request.form['key']
    key = {'_id': ObjectId(result) }
    # print(key)
    db.task.delete_one(key)
    
    return redirect('/list')

@app.route('/edit_task', methods=['POST'])
def update():
    # Update a task

    result= request.form['key']
    name= request.form['task']
    key= {'_id': ObjectId(result) }
    
    db.task.update_one(key, {'$set': {'task':name}})
    return redirect('/list')

if __name__ == '__main__': 
    app.run(port=5000, debug=True) 